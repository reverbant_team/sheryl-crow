<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tinman
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area">
	<?php if(strpos($_SERVER['REQUEST_URI'], "/product-category/") !== false): ?>
		<div class="product-sub-categories">
			<h2>Sub-Categories</h2>
			<ul>
				<?php

				$urlArray = explode('/', $_SERVER['REQUEST_URI']);
				$curCategory = $urlArray[3];

				if($curCategory != null):
					$taxonomy     = 'product_cat';
					$orderby      = 'name';
					$show_count   = 0;      // 1 for yes, 0 for no
					$pad_counts   = 0;      // 1 for yes, 0 for no
					$hierarchical = 1;      // 1 for yes, 0 for no
					$title        = '';
					$empty        = 0;

					$args = array(
						'taxonomy'     => $taxonomy,
						'orderby'      => $orderby,
						'show_count'   => $show_count,
						'pad_counts'   => $pad_counts,
						'hierarchical' => $hierarchical,
						'title_li'     => $title,
						'hide_empty'   => $empty
					);
					$all_categories = get_categories( $args );
					foreach ($all_categories as $cat) {
						if($cat->category_parent == 0 && $cat->slug == $curCategory) {
							$category_id = $cat->term_id;
							//if($cat->name != 'Uncategorized')
							//	echo '<a class="dropdown-item" href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a>';

							$args2 = array(
								'taxonomy'     => $taxonomy,
								'child_of'     => 0,
								'parent'       => $category_id,
								'orderby'      => $orderby,
								'show_count'   => $show_count,
								'pad_counts'   => $pad_counts,
								'hierarchical' => $hierarchical,
								'title_li'     => $title,
								'hide_empty'   => $empty
							);
							$sub_cats = get_categories( $args2 );
							if($sub_cats) {
								foreach($sub_cats as $sub_category) {
									echo  '<li><a href="/wp_tinman/product-category/'. $curCategory . '/' . $sub_category->slug .  '/">' . $sub_category->name . '</a></li>';
								}
							}
						}
					}
				endif; ?>
			</ul>
		</div>
	<?php endif; ?>
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside><!-- #secondary -->
