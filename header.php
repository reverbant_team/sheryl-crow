<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tinman
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'tinman' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif;
			$tinman_description = get_bloginfo( 'description', 'display' );
			if ( $tinman_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $tinman_description; /* WPCS: xss ok. */ ?></p>
			<?php endif; ?>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'tinman' ); ?></button>
			<?php
			//wp_nav_menu( array(
			//	'theme_location' => 'menu-1',
			//	'menu_id'        => 'primary-menu',
			//) );
			?>
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav">
						<li class="nav-item active">
							<a class="nav-link" href="/wp_tinman/">Home <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarProductMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Products</a>
							<div class="dropdown-menu" aria-labelledby="navbarProductMenu">
								<?php

								$taxonomy     = 'product_cat';
								$orderby      = 'name';
								$show_count   = 0;      // 1 for yes, 0 for no
								$pad_counts   = 0;      // 1 for yes, 0 for no
								$hierarchical = 1;      // 1 for yes, 0 for no
								$title        = '';
								$empty        = 0;

								$args = array(
									'taxonomy'     => $taxonomy,
									'orderby'      => $orderby,
									'show_count'   => $show_count,
									'pad_counts'   => $pad_counts,
									'hierarchical' => $hierarchical,
									'title_li'     => $title,
									'hide_empty'   => $empty
								);
								$all_categories = get_categories( $args );
								foreach ($all_categories as $cat) {
									if($cat->category_parent == 0) {
										$category_id = $cat->term_id;
										if($cat->name != 'Uncategorized')
											echo '<a class="dropdown-item" href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a>';

										/*
										 * This is for the sub-categories
										 * TODO: The following code will display the sub-categories. But styling still needs to happen here.
										$args2 = array(
											'taxonomy'     => $taxonomy,
											'child_of'     => 0,
											'parent'       => $category_id,
											'orderby'      => $orderby,
											'show_count'   => $show_count,
											'pad_counts'   => $pad_counts,
											'hierarchical' => $hierarchical,
											'title_li'     => $title,
											'hide_empty'   => $empty
										);
										$sub_cats = get_categories( $args2 );
										if($sub_cats) {
											foreach($sub_cats as $sub_category) {
												echo  $sub_category->name ;
											}
										}*/
									}
								}
								?>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="bandDropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Bands
							</a>
							<div class="dropdown-menu" aria-labelledby="bandDropdownMenu">
								<?php
								// Get the product tags to generate the sublevel menu
								$args = array(
									'number'     => $number,
									'orderby'    => $orderby,
									'order'      => $order,
									'hide_empty' => $hide_empty,
									'include'    => $ids
								);
								$product_tags = get_terms( 'product_tag', $args );
								$term_array = array();
								if ( ! empty( $product_tags ) && ! is_wp_error( $product_tags ) ){
									foreach ( $product_tags as $term ) {
										//$term_array[] = $term->name;
										$band_name = $term->name;
										$band_name_slug = str_replace(" ", "-", strtolower($band_name));
										echo '<a class="dropdown-item" href="/wp_tinman/bands/' . $band_name_slug . '">' . $band_name . '</a>';
									}
								}

								?>
							</div>
						</li>
					</ul>
				</div>
			</nav>

			<?php

			do_shortcode('[woo_products_by_tags tags="Metallica"]');

			?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
