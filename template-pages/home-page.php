<?php

/* Template Name: Home Page - Landing */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

			// Prep all ACF fields for usage
			$bgImg = get_field( "background_image" );
			$bgColor = get_field( "background_color" );

			if( $bgImg ) {
				echo $bgImg;
			} else {
				echo $bgColor;
			}

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
