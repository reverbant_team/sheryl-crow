<?php
/**
 * tinman functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package tinman
 */

if ( ! function_exists( 'tinman_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function tinman_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on tinman, use a find and replace
		 * to change 'tinman' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'tinman', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'tinman' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'tinman_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'tinman_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function tinman_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'tinman_content_width', 640 );
}
add_action( 'after_setup_theme', 'tinman_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function tinman_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'tinman' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'tinman' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'tinman_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function tinman_scripts() {
	wp_enqueue_style( 'tinman-style', get_stylesheet_uri() );

	wp_enqueue_script( 'tinman-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'tinman-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'tinman_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

/**
 * Tinman Updates
 */
function tinman_change_post_menu_label() {
    global $menu;
    global $submenu;
    // Debugging for menus
    //echo '<pre>'; print_r($menu); echo '</pre>';
    //echo '<pre>'; print_r($menu); echo '</pre>';
    $menu[20][0] = 'Band Pages';
    $submenu['edit.php?post_type=page'][5][0] = 'All Bands';

    remove_menu_page('edit.php');
    remove_menu_page('media.php');
    remove_menu_page('links.php');
    remove_menu_page('edit-comments.php');
    //remove_menu_page('themes.php');
    //remove_menu_page('plugins.php');
    //remove_menu_page('tools.php');
    //remove_menu_page('options-general.php');
}
add_action( 'admin_menu', 'tinman_change_post_menu_label' );

function woo_products_by_tags_shortcode( $atts, $content = null ) {

    // Get attribuets
    extract(shortcode_atts(array(
        "tags" => ''
    ), $atts));

    ob_start();
    // Define Query Arguments
    $args = array(
        'post_type' 	 => 'product',
        'posts_per_page' => 5,
        'product_tag' 	 => $tags
    );

    // Create the new query
    $loop = new WP_Query( $args );

    // Get products number
    $product_count = $loop->post_count;

    // If results
    if( $product_count > 0 ) :

        echo '<ul class="products">';

        // Start the loop
        while ( $loop->have_posts() ) : $loop->the_post(); global $product;

            global $post;

            echo "<p>" . $thePostID = $post->post_title. " </p>";

            if (has_post_thumbnail( $loop->post->ID ))
                echo  get_the_post_thumbnail($loop->post->ID, 'shop_catalog');
            else
                echo '<img src="'.$woocommerce->plugin_url().'/assets/images/placeholder.png" alt="" width="'.$woocommerce->get_image_size('shop_catalog_image_width').'px" height="'.$woocommerce->get_image_size('shop_catalog_image_height').'px" />';

        endwhile;

        echo '</ul><!--/.products-->';

    else :

        _e('No product matching your criteria.');

    endif; // endif $product_count > 0

    return ob_get_clean();
}
add_shortcode("woo_products_by_tags", "woo_products_by_tags_shortcode");
